import React from 'react'
import {BrowserRouter,Routes,Route} from 'react-router-dom';
import Layout from './component/Layout/Layout';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import PageNotFound from './pages/PageNotFound';
import Menu from './pages/Menu';
const App = () => {

  return (
    <div>
      <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />}/>
        <Route path='/about' element={<About />}/>
        <Route path='/contect' element={<Contact />}/>
        <Route path='/menu' element={<Menu/>}/>
        <Route path='*' element={<PageNotFound/> }/>
      </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App
