import React from 'react'
import Layout from '../component/Layout/Layout'
import { Link } from 'react-router-dom'
import Banner from '../component/images/Banner2.png'
import '../style/HomeStyle.css'

const Home = () => {
  return (
    <Layout>
        <div className='home' 
        style={{ backgroundImage: `url(${Banner})`}}
        >
            <div className='headrContainer'>
               <h1>Food Website</h1>
               <p>Best Food In India</p>
               <Link to='/menu'>
               <button>
                 ORDER NOW
                </button> 
               </Link>
            </div>
        </div>
    </Layout>
  )
}

export default Home