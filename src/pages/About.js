import React from "react";
import Layout from "../component/Layout/Layout";
import { Typography , Box } from "@mui/material";

const About = () => {
  return (
    <Layout>
      <Box sx={{ 
        my:5,
        textAlign: "center",
        p:2,
        "& h4":{
          fontweigth:'bold',
          my:2,
          fontsize:"2rem",
        },
        "& p":{
          textAlign:"justify",
        },
        "@media (max-width:600px)":{
          mt:0,
          "& h4":{
           fontsize:"2rem"
          }
        }
      }}>
        <Typography variant="h4">Welcome To My Resturant</Typography>
        <p>
          Fast food is a type of mass-produced food designed for commercial
          resale, with a strong priority placed on speed of service. It is a
          commercial term, limited to food sold in a restaurant or store with
          frozen, preheated or precooked ingredients and served in packaging for
          take-out/take-away. Fast food was created as a commercial strategy to
          accommodate large numbers of busy commuters, travelers and wage
          workers. In 2018, the fast food industry was worth an estimated $570
          billion globally.
        </p>
        <br />
        <p>
          The fastest form of "fast food" consists of pre-cooked meals which
          reduce waiting periods to mere seconds. Other fast food outlets,
          primarily hamburger outlets such as McDonald's, use mass-produced,
          pre-prepared ingredients (bagged buns and condiments, frozen beef
          patties, vegetables which are prewashed, pre-sliced, or both; etc.)
          and cook the meat and french fries fresh, before assembling "to
          order". Fast food restaurants are traditionally distinguished by the
          drive-through. Outlets may be stands or kiosks, which may provide no
          shelter or seating,[2] or fast food restaurants (also known as quick
          service restaurants).[3] Franchise operations that are part of
          restaurant chains have standardized foodstuffs shipped to each
          restaurant from central locations.
        </p>
        <br />
        <p>
          The fastest form of "fast food" consists of pre-cooked meals which
          reduce waiting periods to mere seconds. Other fast food outlets,
          primarily hamburger outlets such as McDonald's, use mass-produced,
          pre-prepared ingredients (bagged buns and condiments, frozen beef
          patties, vegetables which are prewashed, pre-sliced, or both; etc.)
          and cook the meat and french fries fresh, before assembling "to
          order". Fast food restaurants are traditionally distinguished by the
          drive-through. Outlets may be stands or kiosks, which may provide no
          shelter or seating,[2] or fast food restaurants (also known as quick
          service restaurants).[3] Franchise operations that are part of
          restaurant chains have standardized foodstuffs shipped to each
          restaurant from central locations.
        </p>
      </Box>
    </Layout>
  );
};

export default About;
