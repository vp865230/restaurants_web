import React from "react";
import Layout from "../component/Layout/Layout";
import {
  Box,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import SupportAgentIcon from "@mui/icons-material/SupportAgent";
import EmailIcon from '@mui/icons-material/Email';
import CallIcon from '@mui/icons-material/Call';

const Contact = () => {
  return (
    <Layout>
      <Box
        sx={{
          m: 5,
          "& h4": {
            fontwidth: "bold",
            mb: 2,
          },
        }}
      >
        <Typography variant="h4">Contect My Resturant</Typography>
        <p>
          The fastest form of "fast food" consists of pre-cooked meals which
          reduce waiting periods to mere seconds. Other fast food outlets,
          primarily hamburger outlets such as McDonald's, use mass-produced,
          pre-prepared ingredients (bagged buns and condiments, frozen beef
          patties, vegetables which are prewashed, pre-sliced, or both; etc.)
          and cook the meat and french fries fresh, before assembling "to
          order". Fast food restaurants are traditionally distinguished by the
          drive-through. Outlets may be stands or kiosks, which may provide no
          shelter or seating,[2] or fast food restaurants (also known as quick
          service restaurants).[3] Franchise operations that are part of
          restaurant chains have standardized foodstuffs shipped to each
          restaurant from central locations.
        </p>
      </Box>
      <Box sx={{ m:3, "@media (max-width:600px)":{
           width:'300px',
          m:5,   
        } 
    }}>
        <TableContainer component={Paper}>
          <Table aria-label="contect table">
            <TableHead>
              <TableRow>
                <TableCell sx={{ bgcolor:'black',color:'white'}} align="center">contect Details</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              <TableRow>
                <TableCell>
                  <SupportAgentIcon sx={{ color:'red',pt:1}}/> 18000-000-0000
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <EmailIcon sx={{ color:'skyblue',pt:1}}/> Help@gmail.com
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell>
                  <CallIcon sx={{ color:'green',pt:1}}/> 1234567890 (tollfree)
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
        </TableContainer>
      </Box>
    </Layout>
  );
};

export default Contact;
