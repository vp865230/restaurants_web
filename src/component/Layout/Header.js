import React from "react";
import {
  AppBar,
  Box,
  Button,
  Toolbar,
  Typography,
  Stack,
  IconButton,
  Drawer,
  drawerClasses,
  Divider,
} from "@mui/material";
import Logo from '../../images/logo.svg'
import { useState } from "react";
import FastfoodIcon from "@mui/icons-material/Fastfood";
import RestaurantMenuIcon from "@mui/icons-material/RestaurantMenu";
import { Link } from "react-router-dom";
import "../../style/HeaderStyle.css";

const Header = () => {
  const [mobileopen, setMobileopen] = useState(false);

  const handelDrawerToggle = () => {
    setMobileopen(!mobileopen);
  };

  //menu drawer
  const drawer = (
    <Box onClick={handelDrawerToggle} sx={{ textAlign: "center" }}>
      <Typography
        color="goldenrod"
        variant="h6"
        component="div"
        sx={{ flexGrow: 1 , my:2}}
      >
        {/* <FastfoodIcon />
        My Hotel */}
        <img src={Logo} alt="logo"  height='70' width='250'/>
      </Typography>
      <Divider/>
      <ul className="mobile-navigation">
        <li>
          <Link to={"/"}>Home</Link>
        </li>
        <li>
          <Link to={"/menu"}>Menu</Link>
        </li>
        <li>
          <Link to={"/about"}>About</Link>
        </li>
        <li>
          <Link to={"/contect"}>Contect</Link>
        </li>
      </ul>
    </Box>
  );

  return (
    <>
      <Box>
        <AppBar component="nav" sx={{ bgcolor: "black" }}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              sx={{
                mr: 2,
                display: { sm: "none" } ,
  
              }}
              onClick={handelDrawerToggle}
            >
              <RestaurantMenuIcon />
            </IconButton>
            <Typography
              color="goldenrod"
              variant="h6"
              component="div"
              sx={{ flexGrow: 1 }}
            >
              {/* <FastfoodIcon />
              My Hotel */}
              <img src={Logo} alt="logo"  height='70' width='250'/>
            </Typography>
            <Box sx={{ display: { xs: "none", sm: "block" } }}>
              <ul className="navigation-menu">
                <li>
                  <Link to={"/"}>Home</Link>
                </li>
                <li>
                  <Link to={"/menu"}>Menu</Link>
                </li>
                <li>
                  <Link to={"/about"}>About</Link>
                </li>
                <li>
                  <Link to={"/contect"}>Contect</Link>
                </li>
              </ul>
            </Box>
          </Toolbar>
        </AppBar>
        <Box component="nav">
          <Drawer
            variant="temporary"
            anchor='bottom'
            open={mobileopen}
            onClose={handelDrawerToggle}
            sx={{  bgcolor:'lightblack' , display:{ xs:'block',sm:'none'}, "& .MuiDrawer-paper ":{
              boxSizing:'border-box',
            }}}
          >
            {drawer}
          </Drawer>
        </Box>
        <Box>
        <Toolbar />
        </Box>
      </Box>
    </>
  );
};

export default Header;
